<?php

    function checkValidString($str) {
        return stripos($str, 'book') xor stripos($str, 'restaurant');
    }

    function countSentence($str) {
        return substr_count($str, '.');
    }

    $file1 = fopen(__DIR__."/file1.txt", 'r');
    $file2 = fopen(__DIR__."/file2.txt", 'r');
    if ($file1 && $file2) {
        $file1Content = fread($file1, filesize(__DIR__."/file1.txt"));
        $file2Content = fread($file2, filesize(__DIR__."/file2.txt"));
        if (checkValidString($file1Content)) {
            echo 'Chuỗi hợp lệ. Chuỗi bao gồm ' . countSentence($file1Content) . ' câu.';
        } else {
            echo 'Chuỗi không hợp lệ';
        }
        echo "\n";
        if (checkValidString($file2Content)) {
            echo 'Chuỗi hợp lệ. Chuỗi bao gồm ' . countSentence($file2Content) . ' câu.';
        } else {
            echo 'Chuỗi không hợp lệ';
        }
        fclose($file1);
        fclose($file2);
    } else {
        if (!$file1) {
            echo 'Lỗi mở file 1' . "\n";
        }
        if (!$file2) {
            echo 'Lỗi mở file 2';
        }
    }
