<?php

class ExerciseString 
{
    private $check1;
    private $check2;

    public function readFile($fileName) 
    {
        $file = fopen($fileName, 'r');
        if ($file) {
            $fileContent = fread($file, filesize($fileName));
            fclose($file);
            return $fileContent;
        } else {
            echo 'Lỗi mở file' . "\n";
        }
    }

    public function checkValidString($str) 
    {
        return stripos($str, 'book') xor stripos($str, 'restaurant');   
    }

    public function writeFile($fileName, $content) 
    {
        $file = fopen($fileName, 'w');
        if ($file) {
            fwrite($file, $content);
            fclose($file);
        } else {
            echo 'Lỗi mở file' . "\n";
        }
    }

    public function getCheck1() 
    {
        return $this->check1;
    }

    public function setCheck1($check1): self 
    {
        $this->check1 = $check1;
        return $this;
    }

    public function getCheck2() 
    {
        return $this->check2;
    }

    public function setCheck2($check2): self 
    {
        $this->check2 = $check2;
        return $this;
    }
}

$object1 = new ExerciseString();
$file1Content = $object1->readFile(__DIR__ . '/file1.txt');
$object1->setCheck1($object1->checkValidString($file1Content));
$file2Content = $object1->readFile(__DIR__ . '/file2.txt');
$object1->setCheck2($object1->checkValidString($file2Content));
$resultFileContent = '- check1 là chuỗi ' . ($object1->getCheck1() ? 'Hợp lệ.' : 'Không hợp lệ.') . "\n"
    . '- check2 là chuỗi ' . ($object1->getCheck2() ? 'Hợp lệ. ' : 'Không hợp lệ. ') . 'Chuỗi có ' . substr_count($file2Content, '.') . ' câu.';
$object1->writeFile(__DIR__ . '/result_file.txt', $resultFileContent);
