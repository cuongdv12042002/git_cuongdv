<?php

function checkValidString($str1, $str2) 
{
    return stripos($str1, $str2) !== false;
}

abstract class Country 
{
    protected $slogan;

    abstract public function sayHello();

    public function getSlogan() 
    {
        return $this->slogan;
    }

    public function setSlogan($slogan): self 
    {
        $this->slogan = $slogan;
        return $this;
    }
}

interface Boss 
{
    public function checkValidSlogan();
}

class EnglandCountry extends Country implements Boss 
{
    use Active;

    public function defindYourSelf() 
    {
        return $this->getClass();
    }

    public function sayHello() 
    {
        echo 'Hello';
    }

    public function checkValidSlogan() 
    {
        return checkValidString($this->slogan, 'England') || checkValidString($this->slogan, 'english');
    }
}

class VietNamCountry extends Country implements Boss 
{
    use Active;

    public function defindYourSelf() 
    {
        return $this->getClass();
    }

    public function sayHello() 
    {
        echo 'Xin chao';
    }

    public function checkValidSlogan() 
    {
        return checkValidString($this->slogan, 'vietnam') && checkValidString($this->slogan, 'hust');
    }
}

trait Active 
{
    public function getClass() 
    {
        return get_class($this);
    }
}

$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();
$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');
$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world.');
$englandCountry->sayHello(); 
echo "<br>";
$vietnamCountry->sayHello(); 
echo "<br>";
var_dump($englandCountry->checkValidSlogan());
echo "<br>";
var_dump($vietnamCountry->checkValidSlogan());
echo 'I am ' . $englandCountry->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $vietnamCountry->defindYourSelf();
