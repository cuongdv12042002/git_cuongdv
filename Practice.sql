USE sampleDB;
-- 1.
SELECT 
    categories.*,
    COUNT(items.category_id) as it_count
FROM
    categories
    LEFT JOIN items
    ON categories.id = items.category_id
GROUP BY categories.id;

-- 2.
SELECT 
    categories.*,
    sum(items.amount) as it_amount
FROM
    categories
    LEFT JOIN items
    ON categories.id = items.category_id
GROUP BY categories.id;

-- 3.
SELECT DISTINCT
    categories.*
FROM
    categories
    INNER JOIN items
    ON categories.id = items.category_id
WHERE items.amount > 40;

-- 4.
DELETE FROM categories
WHERE categories.id IN (
    SELECT id FROM ( 
    SELECT categories.id 
    FROM
        categories
        LEFT JOIN items
        ON categories.id = items.category_id
    GROUP BY categories.id
    HAVING COUNT(items.category_id) = 0) as t
);
